from django.shortcuts import render
from carburant.models import Supply, Card


# Create your views here.
def period_printer(request):
    template_name = 'search/queries_search_supplies_period.html'
    min_date = request.GET.get('minDate')
    max_date = request.GET.get('maxDate')
    if 'card' in request.GET:
        card = Card.objects.get(card_number=request.GET.get('card'))
        if max_date and min_date:
            query = Supply.objects.filter(
                supply_date__lte=max_date,
                supply_date__gte=min_date,
                card=card
            )
            print("Date 1", min_date, "Date 2", max_date, "Carte:", card, type(card), query)

        else:
            query = Supply.objects.none()

    else:
        if max_date and min_date:
            query = Supply.objects.filter(
                supply_date__lte=max_date,
                supply_date__gte=min_date,
            )
        else:
            query = Supply.objects.none()

    context = {
        "queries": query,
        "min_date": min_date,
        "max_date": max_date
    }
    print(context)
    return render(request, template_name, context)
