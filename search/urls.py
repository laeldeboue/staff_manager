from django.urls import path
from search.views import period_printer

app_name = "search"
urlpatterns = [
    path('', period_printer, name='search'),
]