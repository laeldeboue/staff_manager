from django.apps import AppConfig


class CarburantConfig(AppConfig):
    name = 'carburant'
