from django.shortcuts import render, reverse, HttpResponse, get_object_or_404
from django.utils import timezone as tz
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.db.models.aggregates import Sum
from carburant.func_utils import format_registration
from carburant.forms import SupplyForm, VehicleForm, CardForm
from carburant.models import Card, Supply, RechargeCard, Vehicle, CardProvider
from weasyprint import HTML, CSS
import tempfile, csv
from django.conf import settings

today = tz.now()


def home(request):
    template = 'carburant/index.html'
    nb_card = Card.objects.count()
    nb_supply = Supply.objects.filter(
        supply_date__year=today.year,
        supply_date__month=today.month
    ).count()
    vehicles = Vehicle.objects.count()
    context = {
        'cards_count': nb_card,
        'supplies_count': nb_supply,
        'vehicles_count': vehicles,
    }
    return render(request, template, context)


def vehicule_list(request):
    vehicules = Vehicle.objects.filter(is_service_vehicle=True)
    template = "carburant/vehicles.html"
    context = {'vehicles': vehicules}
    return render(request, template, context)


class SupplyListView(ListView):
    template_name = 'carburant/supplies.html'
    model = Supply
    context_object_name = 'supplies'

    def get_queryset(self):
        queryset = Supply.objects.filter(
            supply_date__year=today.year,
            supply_date__month=today.month
        )
        return queryset.order_by('-supply_date')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SupplyListView, self).get_context_data(**kwargs)
        context['today'] = today
        return context


class CardListView(ListView):
    template_name = 'carburant/cards.html'
    model = Card
    context_object_name = 'cards'

    def get_queryset(self):
        return Card.objects.order_by('-date_save')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # self.
        return context


class DetailCardView(DetailView):
    template_name = 'carburant/supplies.html'
    model = Card
    context_object_name = 'card'

    def get_context_data(self, **kwargs):
        context = super(DetailCardView, self).get_context_data(**kwargs)
        card_item_recharged_period = RechargeCard.objects.filter(
            recharge_period__month=today.month,
            recharge_period__year=today.year
        ).get(
            card=self.object,
        )
        card_selected = Card.objects.get(card_number=card_item_recharged_period)
        total_amount = Supply.objects.filter(
            card=card_selected,
            supply_date__year=today.year,
            supply_date__month=today.month,
        ).aggregate(amount_expensed=Sum('amount'))

        # Détermination du solde de la carte
        try:
            balance = self.object.initial_amount - total_amount.get('amount_expensed', 0)
        except TypeError:
            balance = self.object.initial_amount
            total_amount = 0
        context['consumption'] = total_amount
        context['balance'] = balance

        #----------------------------------------------#
        # Mise à jour de la valeur du solde dans la BDD
        Card.objects.filter(card_number=self.object).update(balance=balance)
        #----------------------------------------------#
        return context


#*********************************************************************
#   FORM CREATOR    #
class CreateSupplyView(CreateView):
    model = Supply
    form_class = SupplyForm
    template_name = 'carburant/form.html'
    success_url = reverse_lazy('supplies')

    def form_valid(self, form):
        registration = form.cleaned_data.get('vehicle')
        vehicle, created = Vehicle.objects.get_or_create(registration=format_registration(registration))
        form.instance.vehicle = vehicle
        return super(CreateSupplyView, self).form_valid(form)

    def form_invalid(self, form):
        # obj, created = Vehicle.objects.get_or_create(registration=vehicle)
        # print("VEH =>", obj.id, created)
        print(form.errors)

    def get_context_data(self, **kwargs):
        context = super(CreateSupplyView, self).get_context_data(**kwargs)
        return context


class CreateVehicleView(CreateView):
    form_class = VehicleForm
    model = Vehicle
    template_name = 'carburant/vehicle_form.html'
    success_url = reverse_lazy('home')


class CreateCardView(CreateView):
    form_class = CardForm
    model = Card
    template_name = 'carburant/card_form.html'
    success_url = reverse_lazy('cards')


class SupplyUpdateView(UpdateView):
    model = Supply
    # form_class = SupplyForm
    template_name = 'carburant/form.html'
    fields = ['vehicle', 'card', 'ticket', 'volume', 'amount', 'service',
              'observation', 'signature', 'supply_date', 'odometer']
    # slug_url_kwarg = "pk"
    # slug_field = 'id'

    def get_success_url(self):
        return reverse_lazy('supplies')

    def form_valid(self, form):
        registration = form.cleaned_data.get('vehicle')
        vehicle, created = Vehicle.objects.get_or_create(registration=registration)
        form.instance.vehicle = vehicle
        return super().form_valid(form)

    # def get_queryset(self, form):
    #     queryset = Supply.objects.get(pk=form.instance.pk)
    #     vehicle = Vehicle.objects.get(registration__)
        # return Supply.objects.get(pk=form.instance.pk)


class SupplyDeleteView(DeleteView):
    model = Supply

    def get_success_url(self):
        return reverse_lazy('supplies')


def get_data(pk=None):
    card = get_object_or_404(Card, pk=pk)
    supplies_for_card = Supply.objects.filter(
        card=card,
        supply_date__month=today.month,
        supply_date__year=today.year,
    )
    consumption_card = Supply.objects.filter(
            card=card,
            supply_date__month=today.month,
            supply_date__year=today.year,
        ).aggregate(amount_expensed=Sum('amount'))

    return {"card": card, "supplies": supplies_for_card, "consumption": consumption_card['amount_expensed']}


def print_as_pdf(request, pk):
    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = f"inline; attachment; filename=approv-{tz.now()}.pdf"
    response["Content-Transfer-Encoding"] = 'binary'

    data = get_data(pk=pk)
    html_string = render_to_string("carburant/pdf-output.html",
                                   context={
                                       "total": data['consumption'],
                                       "card": data['card'],
                                       "supplies": data['supplies'],
                                   }
                               )

    html = HTML(string=html_string)
    result = html.write_pdf()

    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output_file = open(output.name, 'rb')
        response.write(output_file.read())

    return response


def convert_to_csv(request, pk):
    supplies = Supply.objects.filter(
        card=pk,
        supply_date__month=today.month,
        supply_date__year=today.year,
    ) #.values()
    data = get_data(pk=pk)

    response = HttpResponse(content_type="text/csv")
    response['Content-Disposition'] = f"attachment;\
        filename='carte-{ data['card'] }-{ today.month-1 }-{ today.year }.csv'"
    response['Content-Transfer-Encoding'] = "binary"

    writer = csv.writer(response)
    writer.writerow(
        ["CARTE", "DATE APPROV.", "TICKET", "QUANTITE", 'MONTANT', 'VEHICULE', 'SERVICE', 'OBSERVATION', "SIGNATURE"])
    # writer.writerow(["2", "Meco", "Deboue", 28, "France"])

    for supply in supplies:
        writer.writerow([supply.card, supply.supply_date, supply.ticket, supply.volume, supply.amount,
                        supply.vehicle, supply.service, supply.observation, supply.signature])

    # AFFICHAGE DES STATISTIQUES
    writer.writerow([""])
    writer.writerow([""])
    writer.writerow([""])
    writer.writerow(["", "DOTATION", "", data['card'].initial_amount])
    writer.writerow(["", "CONSOMMATION", "", data['consumption_card']['amount_expensed']])
    writer.writerow(["", "RESTE", "", data['card'].balance])

    return response
