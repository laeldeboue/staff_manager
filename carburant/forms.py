from django import forms
from carburant.models import Supply, Card, Vehicle, CardProvider


class SupplyForm(forms.ModelForm):
    card = forms.ModelChoiceField(queryset=Card.objects.all())
    vehicle = forms.CharField(strip=True, max_length=12, min_length=5)

    class Meta:
        model = Supply
        fields = ('supply_date', 'card',
                  'ticket', 'volume',
                  'amount', 'service',
                  'odometer',
                  'signature', 'observation')


class VehicleForm(forms.ModelForm):
    class Meta:
        fields = ['registration', 'vin', 'brand', 'model', 'user', 'is_service_vehicle']
        model = Vehicle
        widgets = {
            "registration": forms.TextInput(attrs={
                "placeholder": "Ex: 1234 HB 01"
            }),
            "vin": forms.TextInput(attrs={
                "placeholder": "Ex: LJ11PAB61KC200145"
            }),
            "brand": forms.TextInput(attrs={
                "placeholder": "Ex: Mercedes"
            }),
            "model": forms.TextInput(attrs={
                "placeholder": "Plateau"
            }),
            "user": forms.TextInput(attrs={
                "placeholder": "Ex: Atelier"
            }),
            "is_service_vehicle": forms.CheckboxInput(attrs={
                "data-caption": "Pousser à droite si véhicule de service",
                "data-role": 'switch',
                "class": 'fg-red',
            }),
        }


class CardForm(forms.ModelForm):
    class Meta:
        model = Card
        exclude = ['balance', 'date_save']
