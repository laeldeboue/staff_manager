from string import ascii_letters
from uuid import uuid4
import os, uuid
from datetime import datetime, date

def get_filename_extension(filepath: str):
    basename: str = os.path.basename(filepath)
    filename, file_extension = os.path.splitext(basename)
    return filename, file_extension


def rename_filename(instance, filename):
    now = datetime.now()
    new_file_name: str = str(uuid.uuid4())
    file_name, ext = get_filename_extension(filename)
    final_filename: str = f'justificatifs/' + now.strftime('%Y/%m/%d/') + f'{ new_file_name }{ ext }'
    return final_filename


def format_registration(immat: str):
    position = 0
    if ' ' in immat:
        immat = ''.join(immat.split(' '))
        
    if len(immat) <= 10:
        for seq in immat:
            if seq in ascii_letters:
                position = immat.index(seq)
                break
    registration_num = f'{ immat[:position] } { immat[position:position+2] } { immat[position+2:] }'.upper()
    return registration_num

