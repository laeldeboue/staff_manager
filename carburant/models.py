from django.db import models
from django.shortcuts import reverse
from datetime import datetime
from django.utils.translation import gettext_lazy as _
from simple_history.models import HistoricalRecords
from django.utils import timezone as tz
from carburant.func_utils import format_registration

today = datetime.now()


class CardProvider(models.Model):
    provider_name = models.CharField(_('Fournisseur de la carte'), max_length=20, unique=True)

    def __str__(self):
        return self.provider_name

    class Meta:
        db_table = ''
        managed = True
        verbose_name = _('Fournisseur de carte')
        verbose_name_plural = _('Fournisseurs de carte')


class Card(models.Model):
    card_name: str = models.CharField(_('Nom de la carte'), max_length=50, unique=True)
    card_number: str = models.CharField(_('No de la carte'), max_length=10, unique=True)
    initial_amount: int = models.PositiveBigIntegerField(_("Montant initial"), default=600000)
    balance: int = models.PositiveBigIntegerField(_("Solde"), null=True)
    date_save = models.DateTimeField(auto_now_add=True, editable=False)
    history = HistoricalRecords()

    def __str__(self):
        return self.card_number

    def save(self, **kwargs):
        self.balance = self.initial_amount
        super(Card, self).save(**kwargs)

    def get_absolute_url(self, **kwargs):
        return reverse('supplies_from_card', kwargs={'pk': self.pk, 'card_number': self.card_number})

    class Meta:
        db_table = ''
        managed = True
        verbose_name = _('Carte')
        verbose_name_plural = _('Cartes')
        unique_together = [('card_name', 'card_number'),]


class RechargeCard(models.Model):
    provider = models.ForeignKey(CardProvider, on_delete=models.CASCADE, related_name="recharges",
                                 verbose_name=_("Fournisseur"))
    card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="recharges", verbose_name=_("Carte"))
    amount: int = models.PositiveIntegerField(_("Montant initial"), editable=False, null=True, blank=True)
    recharge_period = models.DateField(_("Date de rechargement"), default=f"{ today.year }-{ today.month }-1")
    date_save = models.DateTimeField(auto_now_add=True, editable=False)
    history = HistoricalRecords()

    def __str__(self):
        return f'{ self.card }'

    def save(self, **kwargs):
        self.amount = self.card.initial_amount
        super().save(**kwargs)

    class Meta:
        db_table = ''
        managed = True
        verbose_name = _('Recharge de carte')
        verbose_name_plural = _('Recharge de cartes')
        unique_together = ('card', 'recharge_period')
        ordering = ['-recharge_period']


class Vehicle(models.Model):
    registration: str = models.CharField(_("Immat. No"), max_length=15, unique=True)
    vin: str = models.CharField(_('No châssis'), max_length=17, null=True, blank=True)
    brand: str = models.CharField(_("Marque du véhicule"), max_length=50, null=True, blank=True)
    model: str = models.CharField(_("Modèle du véhicule"), max_length=30, null=True, blank=True)
    user: str = models.CharField(_("Utilisateur"), default="", max_length=50, null=True, blank=True)
    is_service_vehicle = models.BooleanField(_("Véhicule de service ?"), default=False)

    class Meta:
        db_table = ''
        managed = True
        verbose_name = _('Véhicule')
        verbose_name_plural = _('Véhicules')

    def __str__(self):
        return self.registration

    def save(self, **kwargs):
        self.registration = format_registration(self.registration)
        super().save(**kwargs)


class Supply(models.Model):
    card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='supplies', verbose_name="Carte")
    supply_number: str = models.CharField("No Approv.", max_length=15, editable=False, unique=True)
    supply_date: datetime = models.DateField(_("Date d'approv."))
    ticket: int = models.PositiveBigIntegerField(unique=True)
    volume: float = models.DecimalField(max_digits=5, decimal_places=2)
    amount: int = models.PositiveIntegerField(_("Montant"))
    vehicle: Vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE,
                                         verbose_name=_('Véhicule'), related_name="supplies")
    odometer: float = models.DecimalField(_('Km'), max_digits=12, decimal_places=2, null=True, blank=True)
    service: str = models.CharField(max_length=50)
    observation: str = models.TextField()
    signature: str = models.CharField(max_length=50)
    date_save = models.DateTimeField(auto_now_add=True, editable=False)
    # created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, default=1)
    history = HistoricalRecords()

    def __str__(self):
        return f'{ self.supply_number }'

    def save(self, **kwargs):
        identifier = self.get_timestamp_split()
        self.supply_number = f"{ identifier[:3] }-{ identifier[3:6] }-{ identifier[6:] }"
        self.card.balance -= self.amount
        super().save(**kwargs)

    def get_absolute_url(self, **kwargs):
        return reverse('supply_url', kwargs={'pk': self.pk, 'id_supply': self.supply_number})

    @staticmethod
    def get_timestamp_split():
        timestamp = datetime.timestamp(tz.now())
        tm_split = str(timestamp).split('.')[0]
        return tm_split

    def display_amount_unit(self):
        return f'{self.amount} Fcfa'

    def display_volume_unit(self):
        return f'{self.volume} L'

    class Meta:
        db_table = ''
        managed = True
        verbose_name = _('Approvisionnement')
        verbose_name_plural = _('Approvisionnements')

