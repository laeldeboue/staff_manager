from django.urls import path
from carburant import views


urlpatterns = [
    path('', views.home, name='home'),
    path('cards/', views.CardListView.as_view(), name='cards'),
    path('cards/<int:pk>/<str:card_number>/', views.DetailCardView.as_view(), name='supplies_from_card'),
    path('supplies/', views.SupplyListView.as_view(), name="supplies"),
    path('vehicules/', views.vehicule_list, name="vehicles"),
    path('create-supply/', views.CreateSupplyView.as_view(), name="create_supply"),
    path('update-supply/<int:pk>', views.SupplyUpdateView.as_view(), name="update_supply"),
    path('delete-supply/<int:pk>', views.SupplyDeleteView.as_view(), name="delete_supply"),
    path('export-pdf/<int:pk>/', views.print_as_pdf, name='export_pdf'),
    path('create-vehicle/', views.CreateVehicleView.as_view(), name='new_vehicle'),
    path('create-card/', views.CreateCardView.as_view(), name='new_card'),
    path('csv/<int:pk>/', views.convert_to_csv, name='csv'),
]
