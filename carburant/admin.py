from django.contrib import admin
from carburant.models import Card, RechargeCard, CardProvider, Supply, Vehicle


@admin.register(CardProvider)
class ProviderAdmin(admin.ModelAdmin):
    list_display = ['provider_name']


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ['card_name', 'card_number', 'initial_amount', 'balance']
    # list_filter = ['provider']
    ordering = ('card_name',)


@admin.register(RechargeCard)
class RechargeCardAdmin(admin.ModelAdmin):
    list_display = ['card', 'provider',
                    'amount', 'recharge_period'
                    ]
    list_filter = ['card', 'recharge_period']
    ordering = ('-recharge_period',)


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    list_display = ['registration', 'vin', 'model', 'brand', 'user', 'is_service_vehicle']


@admin.register(Supply)
class SupplyAdmin(admin.ModelAdmin):
    list_display = ['card', 'supply_date',
                    'ticket',
                    'display_volume_unit',
                    'display_amount_unit',
                    'vehicle', 'service',
                    'observation',
                    'signature',
                    ]
    list_filter = ['supply_date']
    Supply.display_volume_unit.short_description = 'Volume'
    Supply.display_amount_unit.short_description = 'Montant'
    # Supply.display_card_used.short_description = "Carte"

    def get_payment_way_text(self, obj):
        payment_way = obj.payment_way.payment_type.get_payment_type_label_display()
        return payment_way

    get_payment_way_text.short_description = "Moy. de paiement"

    # def formfield_for_foreignkey( self, db_field, request, **kwargs ):
    #     if db_field.name == "payment_way":
    #         return PaymentWayChoiceField(queryset=PaymentWay.objects.all())
    #     return super().formfield_for_foreignkey(db_field, request, **kwargs)
