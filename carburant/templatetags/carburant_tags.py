from django import template
from carburant.models import RechargeCard, Card, Supply, Vehicle, CardProvider
from django.db.models.aggregates import Sum
from django.utils import timezone as tz

register = template.Library()
today = tz.now()


@register.inclusion_tag('carburant/template_tags/card_list.html')
def card_list():
    cards = Card.objects.all()
    return { 'card_list': cards }


@register.inclusion_tag('carburant/template_tags/card_dashbord_list.html')
def card_dashboard_list():
    cards = Card.objects.all()
    return { 'card_list': cards }


@register.inclusion_tag('carburant/template_tags/vehicule_list.html')
def vehicle_list():
    vehicles = Vehicle.objects.filter(is_service_vehicle=True)
    return { 'vehicle_list': vehicles }


@register.inclusion_tag('carburant/template_tags/supply_list.html')
def supply_list():
    supplies = Supply.objects.all().order_by('-supply_date', '-date_save')
    return { 'supply_list': supplies }


@register.inclusion_tag('carburant/template_tags/supply_for_this_month.html')
def supplies_of_the_month(card_id):
    supply_list = card_id.supplies.filter(
        supply_date__year=today.year,
        supply_date__month=today.month
    )
    return {'supplies_of_the_month': supply_list}


@register.inclusion_tag('carburant/template_tags/balance_card.html')
def get_balance(card_id):
    # Sélection de la carte pour le mois en cours
    # Sélection de la période du mois en cours
    try:
        card_item_recharged_period = RechargeCard.objects.get(
            card=card_id,
            recharge_period__year=today.year,
            recharge_period__month=today.month,
        )
    except RechargeCard.DoesNotExist:
        card_item_recharged_period = RechargeCard.objects.create(
            provider=CardProvider.objects.get(provider_name="TOTAL"),
            card=card_id,
            # recharge_period=f'{ today.year }-{ today.month }-'
        )
    # Sélection de la carte utilisée
    selected_card = Card.objects.get(card_number=card_item_recharged_period)
    #     Calcul du montant utilisé par les approvisionnements du mois en cours
    total_amount = Supply.objects.filter(
        card=selected_card,
        supply_date__year=today.year,
        supply_date__month=today.month
    ).aggregate(amount_expensed=Sum('amount'))
    #     Détermination du solde de la carte
    try:
        balance = card_id.initial_amount - total_amount.get('amount_expensed', 0)
    except TypeError:
        balance = card_id.initial_amount
    return {'balance': balance}


@register.inclusion_tag('carburant/template_tags/print-supplies.html')
def supplies_for_this_month(card_id):
    supply_list = card_id.supplies.filter(
        supply_date__year=today.year,
        supply_date__month=today.month
    )
    return {'supply_list_of_the_month': supply_list}


@register.simple_tag
def recharge_period(card_id, format_date):
    period = card_id.recharges.get(
        recharge_period__year=today.year,
        recharge_period__month=today.month
    )
    return period.recharge_period.strftime(format_date)
