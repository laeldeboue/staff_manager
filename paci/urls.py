from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('carburant.urls')),
    path('search', include('search.urls', namespace='search')),
]
